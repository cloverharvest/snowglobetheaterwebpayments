# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require "active_record/fixtures"
require "awesome_print"

Event.destroy_all
puts "All events have been cleared."

Performance.destroy_all
puts "All performances have been cleared."

Ticket.destroy_all
puts "All tickets have been cleared."

User.destroy_all
puts "All users have been cleared."

user = CreateAdminService.new.call
Rails.logger.ap("CREATED ADMIN USER: " << user.email)

ActiveRecord::FixtureSet.create_fixtures(
    "#{Rails.root}/spec/fixtures", "events")
ActiveRecord::FixtureSet.create_fixtures(
    "#{Rails.root}/spec/fixtures", "performances")
ActiveRecord::FixtureSet.create_fixtures(
    "#{Rails.root}/spec/fixtures", "tickets")
ActiveRecord::FixtureSet.create_fixtures(
    "#{Rails.root}/spec/fixtures", "users")
