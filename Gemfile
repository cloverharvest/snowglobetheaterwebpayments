source 'https://rubygems.org'
git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby '2.3.0'
gem 'rails', '~> 5.0.1'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'money-rails', '~>1'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'bootstrap-sass'
gem 'devise'
gem 'pg'
gem 'slim-rails', github: 'slim-template/slim-rails'
gem 'awesome_print', '~> 1.7'
gem 'dotenv-rails'
gem 'stripe'
gem 'binding_of_caller'
gem 'babel-transpiler'
gem "sprockets", github: "rails/sprockets"

group :development do
  gem 'better_errors'
  gem 'foreman'
  gem 'rails_layout'
  gem 'spring-commands-rspec'
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'pry-rails'
  gem 'pry-rescue'
  gem 'rspec'
  gem 'rspec-rails'
end

group :test do
  gem "capybara-screenshot"
  gem 'capybara'
  gem 'database_cleaner'
  gem 'fake_stripe'
  gem 'launchy'
  gem 'poltergeist'
  gem 'selenium-webdriver'
  gem 'sinatra', github: 'sinatra/sinatra'
  gem 'vcr'
  gem 'webmock'
end
