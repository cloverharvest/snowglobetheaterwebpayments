Snow Globe Theater
================

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

Snow Globe Theater
================

UN: anna@example.com
PW: changeme

+++++++++++
02/26/17
- End-to-end test for add to cart works
`rspec spec/features/shopping_cart/adds_to_cart_spec.rb`
`rspec spec/workflows/adds_to_cart_spec.rb`

-The unit tests for these models work:
`rspec spec/models/performance_spec.rb`
rspec spec/models/user_spec.rb

-This unit test (ticket_spec) will only work once
`rspec spec/models/ticket_spec.rb`
then we have to:
`bundle exec rails db:test:prepare`
to make it work again
\n
+++++++++++
option  Build a starter application?
choose  Enter your selection: [none]
option  Get on the mailing list for Rails Composer news?
choose  Enter your selection: [mailinglist]
option  Web server for development?
choose  Enter your selection: [puma]
option  Web server for production?
choose  Enter your selection: [puma]
option  Database used in development?
choose  Enter your selection: [postgresql]
option  Template engine?
choose  Enter your selection: [slim]
option  Test framework?
choose  Enter your selection: [rspec]
option  Continuous testing?
choose  Enter your selection: [none]
option  Front-end framework?
choose  Enter your selection: [bootstrap4]
option  Add support for sending email?
choose  Enter your selection: [none]
option  Authentication?
choose  Enter your selection: [devise]
option  Devise modules?
choose  Enter your selection: [default]
option  OmniAuth provider?
choose  Enter your selection: []
option  Authorization?
choose  Enter your selection: [none]
option  Use a form builder gem?
choose  Enter your selection: []
option  Add pages?
choose  Enter your selection: [home]
option  Set a locale?
choose  Enter your selection: [none]
option  Install page-view analytics?
choose  Enter your selection: [none]
option  Add a deployment mechanism?
choose  Enter your selection: [heroku]
option  Set a robots.txt file to ban spiders?
choose  Enter your selection: [true]
option  Create a GitHub repository? (y/n)
choose  Enter your selection: []
option  Add gem and file for environment variables?
choose  Enter your selection: [foreman]
option  Improve error reporting with 'better_errors' during development?
choose  Enter your selection: [true]
option  Use 'pry' as console replacement during development and test?
choose  Enter your selection: [true]
option  Use or create a project-specific rvm gemset?
choose  Enter your selection: []
